package main

import (
	"fmt"
	"math/rand"
	"time"
)

type gameState struct {
	alphabet []rune
	score    int
	round    int
}

func main() {
	// Initialize the game state
	game := gameState{
		alphabet: []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
		score:    0,
		round:    1,
	}

	// Loop until the player wins
	for {
		// Generate a random alphabet to "whack"
		target := game.getRandomAlphabet()

		// Print the game instructions
		fmt.Printf("Round %d: Press the letter %c within 3 second!\n", game.round, target)

		// Get the player's input
		var input rune
		start := time.Now()
		fmt.Scanf("%c\n", &input)
		elapsed := time.Since(start)

		// Check if the player's input is correct and within the time limit
		if input == target && elapsed <= 3*time.Second {
			game.score++
			fmt.Println("Correct!")
		} else {
			fmt.Println("Incorrect or Time's up!")
			break
		}

		// Move on to the next round
		game.nextRound()
	}

	// Print the final score
	fmt.Printf("Game over! Your final score is %d.\n", game.score)
}

// getRandomAlphabet returns a random alphabet from the game's alphabet list
func (g *gameState) getRandomAlphabet() rune {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	return g.alphabet[r.Intn(len(g.alphabet))]
}

// nextRound increments the game's round number
func (g *gameState) nextRound() {
	g.round++
	fmt.Printf("Current score: %d\n", g.score)
	fmt.Printf("Round %d completed!\n", g.round-1)
}
