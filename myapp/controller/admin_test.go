package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdmLogin(t *testing.T) {
	var jsonStr = []byte(`{"Email":"eeeee@gmail.com	", "Password": "eeeeee"}`)
	//create request object
	req, _ := http.NewRequest("POST", "localhost:8080/login", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	//create client
	client := &http.Client{}
	//send request to server
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	//fmt.Print("Hello")
	//check response status
	assert.Equal(t, http.StatusUnauthorized, resp.StatusCode)
	assert.JSONEq(t, `{"error":"sql: no rows in result set"}`, string(body))
}
