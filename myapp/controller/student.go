package controller

import (
	"database/sql"
	"encoding/json"
	"mygo/myapp/model"
	"mygo/myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddStudent(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	var stud model.Student
	// fmt.Println(stud)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&stud)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
	}
	// fmt.Println(stud)

	saveErr := stud.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
	} else {
		httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"message": "student data added"})
	}
	// fmt.Fprintf(w, "add student handler")
}

// helper function to convert sid string to int
func getUserId(sid string) (int64, error) {
	stdid, err := strconv.ParseInt(sid, 10, 64)
	if err != nil {
		return 0, err
	}
	return stdid, nil
}
func GetStud(w http.ResponseWriter, r *http.Request) {
	//stdid is of type string
	ssid := mux.Vars(r)["sid"]
	//stdid is of type int
	stdid, _ := getUserId(ssid)
	//declaring and assigning
	stud := model.Student{StdId: stdid}
	getErr := stud.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		// httpResp.RespondWithError(w, http.StatusBadRequest, "student not found")
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, stud)
	}
	// fmt.Println(stud)
}

func UpdateStud(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	str_sid := mux.Vars(r)["sid"]
	old_stdid, _ := getUserId(str_sid)

	var stud model.Student
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&stud)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	updateErr := stud.Update(old_stdid)
	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, stud)
	}
}

func DeleteStud(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	sid := mux.Vars(r)["sid"]
	stdId, idErr := getUserId(sid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := model.Student{StdId: stdId}
	err := s.Delete()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetAllStuds(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	students, getErr := model.GetAllStudents()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, students)
}
