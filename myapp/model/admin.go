package model

import "mygo/myapp/datastore/postgres"

type Admin struct {
	FirstName string
	LastName  string
	Email     string
	Password  string
}

const (
	queryInsertAdmin = "INSERT INTO admin(firstname, lastname, email, password) VALUES($1, $2, $3, $4) RETURNING email;"
	queryGetAdmin    = "SELECT email, password FROM admin WHERE email=$1 and password=$2;"
)

func (a *Admin) Create() error {
	err := postgres.Db.QueryRow(queryInsertAdmin, a.FirstName, a.LastName, a.Email, a.Password).Scan(&a.Email)
	return err
	// fmt.Println(a)
}

func (a *Admin) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, a.Email, a.Password).Scan(&a.Email, &a.Password)
}
