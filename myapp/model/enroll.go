package model

import "mygo/myapp/datastore/postgres"

type Enroll struct {
	StdId         int64  `json:"stdid"`
	CourseID      string `json:"cid"`
	Date_Enrolled string `json:"date"`
}

const (
	queryEnrollStd = "INSERT INTO enroll(std_id, course_id, date_enrolled) VALUES($1, $2, $3) RETURNING Std_id;"
)

func (e *Enroll) EnrollStud() error {
	row := postgres.Db.QueryRow(queryEnrollStd, e.StdId, e.CourseID, e.Date_Enrolled)
	err := row.Scan(e.StdId)
	return err
}
