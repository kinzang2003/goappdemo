package routes

import (
	"fmt"
	"log"
	"mygo/myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitializerRoutes() {
	//creating a new router
	router := mux.NewRouter()

	//regsiter handler function with the mux router
	router.HandleFunc("/home", controller.HomeHandler)

	//define another route for name for student
	router.HandleFunc("/home/{myname}", controller.ParameterHandler)

	router.HandleFunc("/student", controller.AddStudent).Methods("POST")
	router.HandleFunc("/student/{sid}", controller.GetStud).Methods("GET")
	router.HandleFunc("/student/{sid}", controller.UpdateStud).Methods("PUT")
	router.HandleFunc("/student/{sid}", controller.DeleteStud).Methods("DELETE")

	router.HandleFunc("/students", controller.GetAllStuds)

	//admin routes
	router.HandleFunc("/signup", controller.Signup).Methods("POST")

	router.HandleFunc("/login", controller.Login).Methods("POST")

	router.HandleFunc("/logout", controller.Logout)

	router.HandleFunc("/course", controller.AddCourse).Methods("POST")
	router.HandleFunc("/course/{cid}", controller.GetCour).Methods("GET")
	router.HandleFunc("/course/{cid}", controller.UpdateCour).Methods("PUT")
	router.HandleFunc("/course/{cid}", controller.DeleteCour).Methods("DELETE")
	router.HandleFunc("/courses", controller.GetAllCour).Methods("GET")

	router.HandleFunc("/enroll", controller.Enroll).Methods("POST")

	//serving the static files
	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)

	// start the http server
	err := http.ListenAndServe(":8080", router)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	log.Println("Application running on port 8080")

	//OR
	//log.Fatal(http.ListenAndServe(":8080", router))
}
